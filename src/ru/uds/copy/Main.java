package ru.uds.copy;

import java.util.ArrayList;

public class Main implements Constants {
    public static void main(String[] args) {
        long before = System.currentTimeMillis();
        new MyThread(IN_FILE, OUT_FILE_ONE).start();
        new MyThread(IN_FILE, OUT_FILE_TWO).start();
        long after = System.currentTimeMillis();
        System.out.println("Параллельно " + ((after - before) / 1000)+"s");

        before = System.currentTimeMillis();
        ArrayList copy = File.copy(IN_FILE);
        File.write(OUT_FILE_ONE, copy);
        File.write(OUT_FILE_TWO, copy);
        after = System.currentTimeMillis();
        System.out.println("Последовательно " + ((after - before) / 1000)+"s");

    }
}