package ru.uds.copy;

import java.io.*;
import java.util.ArrayList;

public class File {
    public static void write(String src, ArrayList copy) {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(src, false))) {
            for (Object string : copy) {
                bufferedWriter.write(string+System.lineSeparator());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList copy(String src) {
        String string;
        ArrayList<String> arrayList = new ArrayList<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(src))) {
            while ((string = bufferedReader.readLine()) != null) {
                arrayList.add(string);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return arrayList;
    }
}