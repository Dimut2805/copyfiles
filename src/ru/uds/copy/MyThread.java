package ru.uds.copy;

public class MyThread extends Thread {
    private String in, out;
    MyThread(String in, String out) {
        this.in = in;
        this.out = out;
    }
    @Override
    public void run() {
        File.write(out, File.copy(in));
    }
}
