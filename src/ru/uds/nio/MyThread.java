package ru.uds.nio;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

public class MyThread extends Thread {
    private Path in, out;

    MyThread(String in, String out) {
        this.in = Paths.get(in);
        this.out = Paths.get(out);
    }

    @Override
    public void run() {
        try {
            Files.copy(in, out, REPLACE_EXISTING);
        } catch (IOException e) {
        }
    }
}
