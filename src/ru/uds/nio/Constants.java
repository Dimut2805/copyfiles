package ru.uds.nio;

public interface Constants {
    String IN_FILE = "src\\ru\\uds\\nio\\in.txt";
    String OUT_FILE_ONE = "src\\ru\\uds\\nio\\outOne.txt";
    String OUT_FILE_TWO = "src\\ru\\uds\\nio\\outTwo.txt";
}
