package ru.uds.nio;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

public class Main implements Constants {
    public static void main(String[] args) {
        long before = System.currentTimeMillis();
        new MyThread(IN_FILE, OUT_FILE_ONE).start();
        new MyThread(IN_FILE, OUT_FILE_TWO).start();
        long after = System.currentTimeMillis();
        System.out.println("Параллельно " + ((after - before) / 1000) + "s");

        before = System.currentTimeMillis();
        Path inFile = Paths.get(IN_FILE);
        Path outOne = Paths.get(OUT_FILE_ONE);
        Path outTwo = Paths.get(OUT_FILE_TWO);
        try {
            Files.copy(inFile, outOne, REPLACE_EXISTING);
            Files.copy(inFile, outTwo, REPLACE_EXISTING);
        } catch (IOException e) {
        }
        after = System.currentTimeMillis();
        System.out.println("Последовательно " + ((after - before) / 1000) + "s");
    }
}